###### tags: `Bachelor - Arbeit`
# Camera Companion Board Build and Verification Guide

## Introduction

This is a guide for the Assembly of the Camera Companion Board PCB. This circuit board is a result of the Bachelor Thesis "Camera Companion Board Design".

The battery input is in this document referenced as VCC and may be measured at the Redcube VCC ESC input terminal. GND may be measured at the Redcube VCC ESC GND terminal.

## PCB assessment
	
This PCB holds several differential pair routed signals, therefore the layer stack has to be considered. Impedance of differential pair traces is influenced among other things, by layer to layer distance. This PCB is optimized for the "JLC2313" stackup by JLCPCB. More details in chapter "Differential Pair Lines" of the thesis document. 

## Assembly and Hardware verification

Before assembly, all components should be ordered and sorted to decrease product placement time. Since small pads are prone to be missed, solder paste should be applied with an automatic solder paste printer or with a stencil. The smallest tip available should be used. Following order must be followed. 
	
Through hole components may not be heat resistant for vapour phase soldering machine!
	

- apply Solder Paste on bottom side
- place components on bottom side
- process in vapour phase re-flow machine
- visual control if components are placed correctly
- apply polyimid tape to secure big components
- apply solder paste on top side
- place components on top side
- process in vapour phase re-flow machine
- visual control for solder bridges
- measure resistance across GND and VCC (for example across ESC input terminals)
- measure in continuity mode if VCC is connected with any Hirose Pin -> could damage compute module
- visual control under the microscope of Hirose connectors and camera connectors -> use a very pointy measureing probe and try to move pins without solder applied
- visual control if all DC/DC regulator pins are connected, or have solder bridges
- apply power (between 12 V and 20 V)
- measure CM4 DC/DC regulator output near L40 (ca. 5.3 V)
- depending on DC/DC enable configuration -> connect GPIO2 and GPIO3 on Raspberry Pi connector with 5 V rail
- measure output voltage of PCIe DC/DC regulator near L60 (ca. 3.3 V)
- measure output voltage of linear DC/DC regulator at U4 (ca. 1.5 V)
- measure output voltage of Pixracer DC/DC regulator near C56. (ca. 5.3 V)
- check if compute module fits onto connectors.
- through hole components be soldered in place (servo connector to motor PCB should be soldered while connected, telemetry connector should be soldered with attached module)

	

## Software verification

Only connect the compute module to the PCB when hardware checks are completed. 

In order to show the device tree log, add to `/boot/config.txt` line `debug=on`. Use `sudo vcdbg log msg` to display devicetree log after reboot.

### Install Operation System
In order to flash a operating system to the compute module, a software supplied by Raspberry Pi, called "rpiboot" is needed. This is the default process, described by Raspberry Pi. 
Read "https://www.raspberrypi.com/documentation/computers/compute-module.html" for more.

- install compute module
- install jumper on "n\_RPIBOOT" pin
- connect with micro USB cable to computer
- start rpiboot on computer
- connect PCB to power
- rpiboot should recognise board and close
- flash operating system, for example using the "Raspberry Pi Imager" (ensure ssh is enabled and wifi information is available, and configure hostname)
- remove power and jumper
- attach power and connect with ssh

### Enable Camera Interfaces

The file `dt-blob.bin` contains varius hardware informations. In order to tell the system where the cameras are attached, this file has to be created. 

- Move to the directory where supplied file `cm4-dt-blob.dts` is located.
- compile file and save it in `/boot/dt-blob.bin` with: 
  `sudo dtc -I dts -O dtb -o /boot/dt-blob.bin cm4-dt-blob.dts`
- reboot
- before enabling camera-legacy-mode search for i2cdevices
    - list i2c interfaces with `ls /dev`
    - search i2c devices on interface 0 with `i2cdetect -y 0`
    - two interfaces should be added, both showing a device connected at 0x64
- enable camera-legacy-mode with `sudo raspi-config`
    - -> Interfacing 
    - -> Camera Legacy Mode
    - -> Enable
    - reboot
- check if raspistill is installed with `raspistill`
    - in case it is not found install with `sudo apt-get install libraspberrypi-bin`
- enter command `vcgencmd get_camera` 
    - should return `supported=2 detected=2`
    - works only in camera legacy mode
    -  if no/only one camera is detected view devicetree log with `sudo vcdbg log msg`
    -  at the bottom information about camera detection is found
    -  not detected probably means i2c connection is broken
-  make pcitures with:
    -  `raspistill -cs 0 -o test0.jpeg`
    -  `raspistill -cs 1 -o test1.jpeg`
    -  when sensor data is not aquired, check differential pair data lanes
- download images with scp and view them

### Humidity Sensor
- move to dirctory with file `searchwing_humidity_overlay.dts` 
    - compile and save it to `/boot/overlays/searchwing_humidity.dtbo` with: `sudo dtc -@ -I dts -O dtb -o /boot/overlays/searchwing_humidity.dtbo searchwing_humidity_overlay.dts`
    - add line to `/boot/config.txt`: `dtoverlay=searchwing_humidity`
    - reboot
- check `i2cdetect -y 6` if no address is displayed: probably faulty i2c connection to Raspberry Pi -> should either be 0x44 or 0x45
- install package `lm-sensors` with `sudo apt-get install lm-sensors`
- print sensor output with `sensors`

### UART5 Mavlink Connection
- in `/boot/config.txt` add line `dtoverlay=uart5`
- Install Mavproxy:
    - `sudo apt-get install python3-dev python3-opencv python3-wxgtk4.0 python3-pip python3-matplotlib python3-lxml python3-pygame`
    - `pip3 install PyYAML mavproxy --user`
    - reboot 
- in directory `~/.local/bin/` execute:
    - `python mavproxy.py --master=/dev/ttyAMA1`
    - heatbeat received, and `mode manual` returns new mode

### Mikrotik PCIe module
- enable overlay in `/boot/config.txt` with line `dtoverlay=pcie-32bit-dma`
- install lshw with `sudo apt-get install lshw`
- list devices with `lshw -C network`
    - -> get pcie interface name wlan0
    - should not be `UNCLAIMED`
- with `ip address` get ip address of interface name wlan0
- connect via ssh and new ip to pi
 - deactivate default wifi interface of cm4: `sudo ifconfig wlan1 down`


### Thermal Camera
In oreder to access the thermal camera, a python library is required. This library depends on opencv. Since compiling and installing opencv takes ages on the RaspberryPi [install a precompiled version](https://lindevs.com/install-precompiled-opencv-on-raspberry-pi/).

- install opencv
- install numpy
    - `sudo apt-get install python-numpy`
- [increase SPI buffer size](https://github.com/groupgets/pylepton/issues/52):
    - edit `/boot/cmdline.txt`, add to end: `spidev.bufsiz= 65535`
    - reboot
    - check success with: `cat /sys/module/spidev/parameters/bufsiz`
- activate and configure SPI interface:
    - go to directory where file called `searchwing_lepton_cam_overlay.dts` is located
    - compile and save it to `/boot/overlays/searchwing_lepton_cam.dtbo` with `sudo dtc -@ -I dts -O dtb -o /boot/overlays/searchwing_lepton_cam.dtbo searchwing_lepton_cam_overlay.dts`
    - add line to `/boot/config.txt`: `dtoverlay=searchwing_lepton_cam`
    - reboot
- [install pylepton](https://github.com/groupgets/pylepton/tree/lepton3-dev)
    - clone repository: `git clone https://github.com/groupgets/pylepton/tree/lepton3-dev`
    - move in repository and install with `sudo python setup.py install`
- move to here supplied directory where file `lepton.py` is located
    - run with `python lepton.py`
